//
//  ViewController.m
//  newAlarm
//
//  Created by Prince on 19/10/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
@interface ViewController ()
{
    int count;
}
@property (strong, nonatomic) IBOutlet UIImageView *imageView1;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePick;
@property (strong, nonatomic) IBOutlet UIButton *startButton;
@property (strong, nonatomic) IBOutlet UIButton *stopButton;
@property (strong, nonatomic) IBOutlet UILabel *label1;

@end

@implementation ViewController
@synthesize imageView1;
@synthesize datePick;
@synthesize startButton;
@synthesize stopButton;
@synthesize label1;
long seconds;

NSTimer *timer;
AVAudioPlayer *music;


- (void)viewDidLoad {
    label1.font=[UIFont fontWithName:@"DBLCDTempBlack" size:43.0];
    datePick.hidden=FALSE;
    datePick.datePickerMode=UIDatePickerModeTime;
    datePick.minuteInterval=1;
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
     - (IBAction)startpressed:(id)sender {
    
     startButton.hidden=TRUE;
     datePick.hidden=TRUE;
    
    NSDate *currentTime=[[NSDate alloc] init];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
       dateFormatter.timeStyle=NSDateFormatterShortStyle;               //gives the time formet like 2:25:45.
   
       dateFormatter.dateStyle=NSDateFormatterShortStyle;              //gives the date formet like 10/5/2015.
    
         NSString *dateTime=[dateFormatter stringFromDate:datePick.date];
         NSLog(@"start button pressed :%@",dateTime);
    
      NSTimeInterval diff = [datePick.date timeIntervalSinceDate:currentTime];   //used to find the difference between current date and the date picker date .
    
    
      seconds= lroundf(diff);
      timer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(targetMethod) userInfo:nil repeats:YES];
    
}

  -(void)targetMethod{          //function targetMethod called.
   
      seconds--;
    
    int hours =(seconds/3600);             //  (time difference) seconds is converted into hours, minutes, seconds.
    int minute=(seconds %3600)/60;
    int secs=seconds %60;

      label1.text=[NSString stringWithFormat:@"%d:%0.2d:%0.2d",hours,minute,secs];     //show houres,min,sec into label
    
      if (seconds==0)       // if second are equal to zero then sound played.
  
 {
        
    NSString *path =[[NSBundle mainBundle] pathForResource:@"rington" ofType:@"mp3"];   //used to play sound
    music =[[AVAudioPlayer alloc] initWithContentsOfURL: [NSURL fileURLWithPath:path] error:nil];
    [music play];
    [timer invalidate ];
        
        }

}

   - (IBAction)stopPressed:(id)sender
  {                                       //close button function decleared.
        label1.text=@"";
        datePick.hidden=false;
        [timer invalidate ];
        [music pause];
        startButton.hidden=FALSE;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
